# Generic Helm Chart

This is a helm chart that's meant to be templated by helmfile for microservice deployments.

And example helmfile would be:

``` yaml
# DOMAIN=example.io ENVIRONMENT=dev helmfile -f helmfile.yaml diff

repositories:
- name: aztek
  url: https://charts.aztek.io

releases:
- name: my_microservice
  namespace: default
  chart: aztek/generic
  version: 0.2.0
  values:
  - image:
      repository: 123456789012.dkr.ecr.us-west-2.amazonaws.com/myproject/microservice
      tag: 7d0dec8
    ingress:
      annotations:
        kubernetes.io/ingress.class: nginx
        external-dns.alpha.kubernetes.io/hostname: {{ requiredEnv "ENVIRONMENT" }}.{{ requiredEnv "DOMAIN" }}
      hosts:
      - host: {{ requiredEnv "ENVIRONMENT" }}.{{ requiredEnv "DOMAIN" }}
        paths:
        - '/this_microservice'
    microservice:
      env:
        AWS_DEFAULT_REGION: us-west-2
  wait: true
  timeout: 30
```
